import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  constructor(private http: HttpClient) { }

  onThrowOut($event) {
    console.log($event)
  }
  stackConfig = {
    
  }
  cards

  fetchUsers() {
    this.http.get('http://localhost:3000/users/')
      .subscribe(users => this.cards = users)
  }

  ngOnInit() {
    this.fetchUsers()
  }


}
